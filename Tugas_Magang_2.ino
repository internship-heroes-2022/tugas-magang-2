/* 
 *  Nama: Muhammad Nabil Akbar Mustafa
 *  Nim: 21/481679/PA/20981
 *  
 *  Saya membuat pengaturan palang otomatis khusus motor jadi
 *  jika ada motor yang lewat maka palang akan naik keatas.
 *  Saya buat palang bergerak keatas jika jarak kurang dari 50 cm
 *  karena rata-rata lebar motor adalah 80cm dan lebar jalan 
 *  khusus motor kurang lebih 1 meter dan saya kira 50cm sudah pas. 
 *  Saya buat delay dari palang turun ke naik sebesar 1 detik dan 
 *  palang dari naik ke turun 2 detik.
 *   
*/

#include <Servo.h> //memasukkan library servo

#define trigpin 3 //set trigger ultasonic transceiver di pin 3
#define echopin 2 //set echo ultasonic transceiver di pin 2

Servo servo_9; //membuat variable servo_9

float durasi; // variable untuk durasi mengeluarkan dan menerima sensor
float jarak; // variable untuk perhitungan jarak

void setup()
{
  servo_9.attach(9); // deklarasi servo pada pin 9
  pinMode(trigpin, OUTPUT); // pin trigger sebagai output
  pinMode(echopin, INPUT); // pin echo sebagai input
  Serial.begin(9600); // set kecepatan komunikasi serial
}

void loop()
{
  pengaturansensor(); // memanggil fungsi pengaturansensor
  delay(100);
  pengaturanpalang(); // memanggil fungsi pengaturanpalang
  delay(100);
}

/* saya buat fungsi baru karena jika tidak sepertinya terlalu berat 
 * dan saya coba sensornya tidak membaca jarak  
*/

void pengaturansensor()
{
  // program trigger untuk memancarkan gelombang ultrasonik
  digitalWrite(trigpin, LOW); 
  delayMicroseconds(2);
  digitalWrite(trigpin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigpin, LOW);
  
  // waktu tunggu pin echo menjadi HIGH dimasukkan kedalam variable durasi setelah
  // menerima pantulan gelombang ultrasonik dari trigger pin 
  durasi = pulseIn(echopin, HIGH);
  jarak = durasi * 0.034 /2; // menghitung jarak 
  
  Serial.print(jarak); // mencetak jarak ke serial monitor
  Serial.println(" cm");
}

void pengaturanpalang()
{
  if(jarak <= 50){ // kontrol palang untuk menaikkan palang jika jarak <= 50 cm 
    servo_9.write(90); // posisi servo 90 derajat
    delay(2000); //membuat delay
  }
  if(jarak > 50){ // kontrol palang untuk menurunkan palang jika jarak > 50 cm
    servo_9.write(0); // posisi servo 0 derajat
    delay(1000); //membuat delay
  }
}
